module bitbucket.org/pcasmath/module

go 1.16

require (
	bitbucket.org/pcasmath/abeliangroup v0.0.1
	bitbucket.org/pcasmath/object v0.0.4
	bitbucket.org/pcasmath/ring v0.0.1
)
